#Claire Pickhardt
#Lab 3
#Problem 5
#21 September 2015
#Honor Code: The work I am submitting is the result of my own thinking and efforts.
#This program changes an ASCII character into an integer using MIPS

	.data
	.align 2
letter: .ascii "C"
c:	.space 4
	
	.text
	lw $t0, letter #puts the character "c" ascii code into register t0
	sub $t1, $t0, 65 #subtracts the ascii code for A from the ascii code for C
	sw $t1, c #stores the value for the ascii code in c
	li $v0, 10
	syscall #terminates the program