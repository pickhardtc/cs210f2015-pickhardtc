﻿Claire Pickhardt
Computer Science 210
8 October 2015
Lab 4 Part 4
Honor Code:The work that I am submitting is the result of my own thinking and efforts.

Integer 1: 45 (binary 01000101)
Integer 2: -50 (binary 11001110)

Logical AND: 44 (binary 01000100)
Logical OR: -49 (binary 11001111)
Logical XOR: -117 (binary 10001011)
Logical right shift of integer 1 two places: 17 (binary 00010001)
Logical left shirt of integer 1 two places: 20 (binary 00010100)
Logical right shift of integer 2 two places: 51 (binary 00110011)
Logical left shift of integer 2 two places: 56 (binary 00111000)
Arithmetic right shift of integer 1 two places: 17 (binary 00010001)
Arithmetic right shift of integer 2 two places: -13 (binary 11110011)
