#Claire Pickhardt
#Lab 3
#Problem 3
#21 September 2015
#Honor Code: The work I am submitting is a result of my own thinking and efforts.
#This program practices with syscalls and also practices with MIPS to compute x=7*y when y=13.

	.data
	.align 2
y:	.word 13
x:	.space 4

	.text
	lw $t0, y #puts the value of integer y into register t0
	add $t1, $t0, $t0 #saves the value for 2y into register t1
	add $t2, $t1, $t1 #saves the value for 4y into register t2
	add $t2, $t2, $t1 #saves the value for 6y into register t2
	add $t0, $t0, $t2 #saves the value for 7y into register t0
	
	li $v0, 10
	syscall