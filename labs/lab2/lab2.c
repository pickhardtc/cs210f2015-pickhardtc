//Claire Pickhardt
//CMPSC 210
//14 September 2015
//Lab 2


//This program is one that takes in two integers and both calculates and returns the values of their sum and their mean.
#include <stdio.h>
#include <math.h>
//function prototype
int mean(double, double);

//main function that will later pass into mean function
int main(){
    int i;
    int j;
    double sum;
    double avg;

    printf("Please enter two numbers: ");
    scanf("%d%d", &i, &j);
    printf("Your two chosen numbers are %d & %d. \n",i,j);
    sum = (i+j);
    printf("Your sum is: %lf\n",sum);
    avg = (sum/2);
    printf("The mean of your numbers is: %lf\n",avg);

    //these parameters will be used to find the mean between the two numbers that the use enters
}//main

//second mean function that calls upon main function
int mean(double sum, double avg){
    return sum;
    return avg;

}//mean function

