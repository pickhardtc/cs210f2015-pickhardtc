//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Claire Pickhardt
//CMPSC 210 Fall 2015
//Lab #1
//counts all the vowels and consonants in the ritchie file and prints their number values at the bottom of the page
#include <stdio.h>
main()
{
    int c=0;
    int vowelc=0;
    int consonantc=0;
    int count=0;

    while((c = getchar()) != EOF){
        if(c=='a')
            vowelc++;

        if (c=='A')
         vowelc++;

        if(c=='e')
        vowelc++;

        if(c=='E')
        vowelc++;

        if(c=='i')
        vowelc++;
        if(c=='I')
        vowelc++;
        if(c=='o')
        vowelc++;
        if(c=='O')
        vowelc++;
        if(c=='u')
        vowelc++;
        if(c=='U')
        vowelc++;
        else if(c==' '||c=='.'||c==','||c=='-'||c=='"'||c=='/'||c==':'){
            count++;
        }
        else{
            consonantc++;
        }

    }//while
        printf("Vowels %d.\n",vowelc);
        printf("Consonants %d.\n",consonantc);

}//main method
