;Claire Pickhardt
;Computer Science 220
;Lab 8, Part 1
;2 November 2015
;Honor Code: The work that I am submitting is the result of my own thinking and efforts.
;This is a NASM program that mimics the following given C program.

;   include <stdio.h>
;   int main(){
;       int r = 119;
;       int s = -32;
;       int t = 7;
;       int u = (r-(s-t)) & (s+t-r);
;   printf("r=%d, s=%d, t=%d, u=%d\n",r,s,t,u);
;   }

    extern printf

section .data    ;constants section

r: dd 119       
s: dd -32
t: dd 7
fmt: db "r=%d,s=%d,t=%d,u=%d",10,0 ;print string

section .bss     ;variable section
u: resb 4       ;reserve one word for u (4 bytes)

section .text    ;program section 
    global main
main:
    push ebp    ;necessary step
    mov ebp,esp ;necessary step

    mov eax,[r] ;enters in contents of r (=119)
    mov ebx,[s] ;enters in contents of s (=-32)
    sub ebx,[t] ; ebx = subtracts contents of t from ebx (=s)
    sub eax, ebx ; eax = subtracts contents of ebx (s-t) from eax(r)
    mov [u],eax ;put all contents of eax into integer u
    mov eax,[t] ;put contents of t into eax
    add eax, ebx ;eax = eax+ebx
    sub eax, [r] ;eax = eax - contents of r
    and [u], eax ;completes arithmetic statement above

;time to print all of this out!
push dword [u]  ;first we push the last argument 
push dword [r]
push dword [s]
push dword [t]
push dword fmt
call printf     ; print statement
add esp,16      ;pushed 4 words, pop 16 bytes

mov esp, ebp ;necessary step
pop ebp ;necessary step

ret ;return from the program



    

    
