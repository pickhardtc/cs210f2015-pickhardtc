#Claire Pickhardt
#Lab 2
#Problem 1
#21 September 2015
#Honor Code: The work that I am submitting is a result of my own thinking and efforts.
#This program carries out a simple math function as outlined in the lab specifications and stores the result in integer x.
	.data
	.align 2
a:	.word 10 #gives an integer a a value of 10
b:	.word 20 #gives an integer b a value of 20
c:	.word -30 #gives an integer c a value of -30
d:	.word -40 #gives an integer d a value fo -40
x:	.space 4 #saves space for a computation result as an integer x

	.text
	lw $t0, a #places integer a into register t0
	lw $t1, b #places integer b into register t1
	lw $t2, c #places integer c into register t2
	lw $t3, d #places integer d into register t3
	add $t4, $t1, $t1 #adds integer b together twice and stores result in register t4
	add $t5, $t4, $t2 #adds integer c to b*2 and stores result in register t5
	sub $t6, $t5, $t3 #subtracts integer d from b*2+c and stores it in register t6
	sub $t7, $t0, $t6 #subtracts b*2+c-d from integer a and stores it in register t7
	sw $t7, x #assigns the value in register t7 to x and stores it in memory