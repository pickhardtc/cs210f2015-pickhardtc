#Claire Pickhardt
#Lab 2
#Problem 2
#21 September 2015
#Honor Code: The work that I am submitting is a result of my own thinking and efforts.
#This program carries out the calculation x=(a-b)*3+(a+b)*6 and stores the result in x.

	.data
	.align 2
a:	.word 10
b:	.word -1
x:	.space 4

	.text
	lw $t0, a
	lw $t1, b
	sub $t2, $t0, $t1 #saves the value of (a-b)
	add $t3, $t0, $t1 #saves the value of (a+b)
	add $t4,$t2, $t2 #saves the value of 2(a-b)
	add $t2, $t4, $t2 #saves the value of 3(a-b) 
	add $t4, $t3, $t3 #saves the value of 2(a+b)
	add $t3, $t4, $t4 #saves the value of 4(a+b)
	add $t3, $t4, $t3 #saves the value of 6(a+b)
	add $t2, $t2, $t3 #calculates whole result
	sw $t2, x