//Claire Pickhardt
//Computer Science 210
//Professor Roos
//7 September 2015

#include <stdio.h>
//a program that numbers lines of a given input
main(){
    int count, newline;

    newline=0;
    count=0;
    while((count = getchar()) !=EOF){
        if(count == '\n'){
            newline++;
            printf("%3c\n",count);
            printf("%3d.",newline);

        }
        else{
            putchar(count);
        }//else
    }//while loop
}//main
