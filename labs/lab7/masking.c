/*Claire Pickhardt
 * Computer Science 210
 * Lab 7
 * Problem 2
 * 26 October 2015
 * Honor Code:The work that I am submitting is the result of my own thinking and efforts.
 * This portion of the lab gets input values from the user and changes their value with masking.
 */

#include <stdio.h>
int main(){
    unsigned int a;
    int begin;
    int end;
    unsigned int mask = 0xFFFFFFFF;

    printf("Enter a value for a.\n");
    scanf("%u", &a);
    printf("Enter a beginning bit value between 0 and 31.\n");
    scanf("%d", &begin);
    printf("Enter an end bit value between 0 and 31.\n");
    scanf("%d", &end);

/*Here starts the masking function that takes in integer a and recognizes it as bits
 * and places a mask on the bits that the user specifies that they don't want to use.*/

    mask=(mask<<begin);
    mask=(mask>>begin);
    a=(a&mask)>>(31-end);
    printf("The new number is: %d\n", a);

}/*main method*/
