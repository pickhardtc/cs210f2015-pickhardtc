#Claire Pickhardt
#Computer Science 210
#Lab 5
#5 October 2015
#Problem 2
#Honor Code: The work that I am submitting is the result of my own thinking and efforts
#This program is designed to get an integer from the user, take that integer and ask for that many more integers (one per line) and find both the maximum and minimum out of the integers inputted and print them out.

	.data
	.align 2
n:	.space 4
max:	.space 4
min:	.space 4
promptn: .asciiz "Please enter an integer n between 1 and 20: "
promptn2: .asciiz "Enter "
promptn3: .asciiz " integers, one per line:"
promptmin: .asciiz "The minimum is "
promptmax: .asciiz ", the maximum is "

	.text
	la $a0, promptn #stores the string that asks the user for an input integer
	li $v0, 4 #prints the prompt string
	syscall #performs the print
	
	li $v0, 5 #reads the int that was input by the user
	syscall #performs the read
	add $s0, $zero, $v0 #puts the value for the integer into register s0
	
	la $a0, promptn2 #stores the string that will string together the next integer prompt
	li $v0, 4 #system call to print a string
	syscall #ends the segment and prints the string
	
	add $a0, $zero, $s0 #puts the integer from register s0 into a0 for printing purposes
	li $v0, 1 #system call for printing an integer
	syscall #prints the integer
	
	la $a0, promptn3 #stores the string value into a0 for printing
	li $v0, 4 #system call for printing a string
	syscall #prints the string
	
	sw $t0, max #stores value for max into  register t0
	sw $t1, min #stores value for min into register t1
	addi $t1, $zero, 2147483647 #assigns a constant value to min so that it has something to compare itself to
	
loop:	beq $s0, $zero, done #if n == 0, skip the loop and go to the minimum
	li $v0, 5 #reads the int that was input by the user
	syscall #performs the read
	add $s1, $zero, $v0 #saves the value of the int into register s1
	
	
	slt $t8, $t0, $s1 #checks if max is less than integer entered
	beq $t8, $zero, minloop #if t0 is less than s1, the value in t8 should be zero and we should go to check the minimum value
	add $t0, $zero, $s1 #places the value for the max into t0
	
minloop: slt $t9, $s1, $t1 #checks to see if s1 < t1
	beq $t9, $zero, skip #if s1 is less than t1, then exit the loop
	add $t1, $zero, $s1 #places the new value of the min into register t1
	
skip: 	addi $s0, $s0, -1
	j loop #end of loop	
done:
	sw $t0, max   #max
	sw $t1, min  #min
	
	la $a0, promptmin #loads in the string to be printed
	li $v0, 4 #prints the prompt string
	syscall #performs the print
	
	add $a0, $zero, $t1 #puts the min value in a0 to be printed
	li $v0, 1 #system call for printing an integer
	syscall #prints the integer
	
	la $a0, promptmax #loads in the string to be printed
	li $v0, 4 #system call for printing a string
	syscall #performs the print
	
	add $a0, $zero, $t0 #loads in the max value into a0 to be printed
	li $v0, 1 #system call for printing an integer
	syscall #performs the print
		
	
	
	li $v0, 10 #system call for ending a program
	syscall #terminates the program
	
	
	
	
	

