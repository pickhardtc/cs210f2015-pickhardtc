//Claire Pickhardt
//Professor Roos
//Computer Science 220
//Lab 2
//14 September 2015
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//program that takes character arrays and reverses them
#include <stdio.h>
main(){
    char a []= "hey";
    char b []= "what's up";
    char c []= "hello";
    int i;//position in array
    int la, lb, lc; //length of array
    la = sizeof(a);
    lb = sizeof(b);
    lc = sizeof(c);

    printf("array c backwards: ");
    for(i=lc; i>=0; i--){
        printf("%c",c[i]);
    }//for c

printf("\narray b backwards: ");
    for(i=lb; i>=0; i--){
        printf("%c",b[i]);
    }//for b

printf("\n array a backwards: ");
    for(i=la; i>=0; i--){
        printf("%c",a[i]);
    }//for a
printf("\n");
    }//revchar


