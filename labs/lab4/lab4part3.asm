#Claire Pickhardt
#Computer Science 220
#Lab 4
#28 September 2015
#Problem 3
#Honor Code: The work that I am submitting is a result of my own thinking and efforts.
#This program is designed practice with input/output operations in MIPS. 

	.data
	.align 2
x:	.space 4
y:	.space 4
promptx: .asciiz "Enter an integer value for x: "
prompty: .asciiz "Enter an integer value for y: "

	.text
	li $v0, 4 #system call for printing a string
	la $a0, promptx #calls string to be printed
	syscall #ends segment and prints string
	
	li $v0, 5 #reads in the integer entered by user
	syscall #performs the read
	add $t0, $zero, $v0 #saves the integer value x into register t0
	
	li $v0, 4 #system call for printing a string
	la $a0, prompty #calls string to be printed
	syscall #ends segment and prints string
	
	li $v0, 5 #reads in the integer entered by user
	syscall #performs the read
	add $t1, $zero, $v0 #saves the integer value y into register t1
	
	and $t2, $t0, $t1 #finds the logical and of x and y entered
	or $t3, $t0, $t1 #finds the logical or of x and y entered
	xor $t4, $t0, $t1 #finds the logical xor of x and y entered 
	sll $t5, $t0, 2 #shifts the value x two places to the left
	sll $t6, $t1, 2 #shifts the value y two places to the left
	srl $t7, $t0, 2 #shifts the value x two places to the right
	srl $t8, $t1, 2 #shifts the value y two places to the right
	sra $t9, $t0, 2 #arithmetic shift of x two places to the right
	sra $t0, $t1, 2 #arithmetic shift of y two places to the right 
	
	li $v0, 10 #system call for exiting the program
	syscall #terminates the program
	
	
