/*Claire Pickhardt
 * Lab 7 Bonus
 * 26 October 2015
 * Honor Code: The work that I am submitting is the result of my own thinking and efforts.
 * The program is designed to let the user know the relative sizes of several different types of identifiers.
 */

#include <stdio.h>
int main(){

    int a;
    float b;
    double c;
    char d;
    short e;
    long f;
    long long g;

    printf("int = %ld bytes\n", sizeof(a));
    printf("float = %ld bytes\n", sizeof(b));
    printf("double = %ld bytes\n", sizeof(c));
    printf("char = %ld bytes\n", sizeof(d));
    printf("short = %ld bytes\n", sizeof(e));
    printf("long = %ld bytes\n", sizeof(f));
    printf("long long = %ld bytes\n",sizeof(g));


}/*main method*/
